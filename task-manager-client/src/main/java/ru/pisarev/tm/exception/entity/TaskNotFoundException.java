package ru.pisarev.tm.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    @NotNull
    public TaskNotFoundException() {
        super("Error. TaskRecord not found.");
    }

}
