package ru.pisarev.tm.listener.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.event.ConsoleEvent;
import ru.pisarev.tm.listener.AbstractListener;
import ru.pisarev.tm.service.PropertyService;

@Component
public class VersionDisplayListener extends AbstractListener {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @Override
    public String name() {
        return "version";
    }

    @Override
    public String arg() {
        return "-v";
    }

    @Override
    public String description() {
        return "Display program version.";
    }

    @Override
    @EventListener(condition = "@versionDisplayListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println(propertyService.getApplicationVersion());
        System.out.println(Manifests.read("build"));
    }
}
