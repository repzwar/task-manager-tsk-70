package ru.pisarev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.endpoint.ProjectRecord;
import ru.pisarev.tm.endpoint.ProjectEndpoint;
import ru.pisarev.tm.event.ConsoleEvent;
import ru.pisarev.tm.exception.entity.ProjectNotFoundException;
import ru.pisarev.tm.listener.ProjectAbstractListener;
import ru.pisarev.tm.util.TerminalUtil;

@Component
public class ProjectStartByIdListener extends ProjectAbstractListener {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Override
    public String name() {
        return "project-start-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Start project by id.";
    }

    @Override
    @EventListener(condition = "@projectStartByIdListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("Enter id");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final ProjectRecord project = projectEndpoint.startProjectById(getSession(), id);
        if (project == null) throw new ProjectNotFoundException();
    }

}
