package ru.pisarev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.endpoint.TaskRecord;
import ru.pisarev.tm.endpoint.TaskEndpoint;
import ru.pisarev.tm.event.ConsoleEvent;
import ru.pisarev.tm.exception.entity.TaskNotFoundException;
import ru.pisarev.tm.listener.TaskAbstractListener;
import ru.pisarev.tm.util.TerminalUtil;

@Component
public class TaskFinishByNameListener extends TaskAbstractListener {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public String name() {
        return "task-finish-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Finish task by name.";
    }

    @Override
    @EventListener(condition = "@taskFinishByNameListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final TaskRecord task = taskEndpoint.finishTaskByName(getSession(), name);
        if (task == null) throw new TaskNotFoundException();
    }

}
