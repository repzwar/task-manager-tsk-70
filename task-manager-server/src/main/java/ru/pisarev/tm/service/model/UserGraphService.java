package ru.pisarev.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pisarev.tm.api.IPropertyService;
import ru.pisarev.tm.api.service.model.IUserService;
import ru.pisarev.tm.exception.empty.*;
import ru.pisarev.tm.exception.entity.EmailExistException;
import ru.pisarev.tm.exception.entity.LoginExistException;
import ru.pisarev.tm.exception.entity.UserNotFoundException;
import ru.pisarev.tm.model.UserGraph;
import ru.pisarev.tm.repository.model.IUserRepository;
import ru.pisarev.tm.util.HashUtil;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserGraphService extends AbstractGraphService<UserGraph> implements IUserService {

    @NotNull
    @Autowired
    private IUserRepository repository;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Override
    @SneakyThrows
    public List<UserGraph> findAll() {
        return repository.findAll();
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<UserGraph> collection) {
        if (collection == null || collection.isEmpty()) return;
        for (UserGraph item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserGraph add(@Nullable final UserGraph entity) {
        if (entity == null) return null;
        repository.save(entity);
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserGraph findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findById(optionalId.orElseThrow(EmptyIdException::new)).orElse(null);
    }

    @Override
    @SneakyThrows
    public void clear() {
        repository.deleteAll();
        @NotNull final List<UserGraph> users = repository.findAll();
        for (UserGraph t :
                users) {
            repository.delete(t);
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @Nullable final UserGraph user = repository
                .findById(optionalId.orElseThrow(EmptyIdException::new)).orElse(null);
        if (user == null) return;
        repository.delete(user);
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final UserGraph entity) {
        if (entity == null) return;
        @Nullable final UserGraph user = repository.findById(entity.getId()).orElse(null);
        if (user == null) return;
        repository.delete(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserGraph findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return repository.findByLogin(login);
    }

    @Override
    @SneakyThrows
    public UserGraph findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return repository.findByEmail(email);
    }

    @Override
    @SneakyThrows
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserGraph user = repository.findByLogin(login);
        if (user == null) return;
        repository.delete(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserGraph add(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (isLoginExist(login)) throw new LoginExistException(login);
        @NotNull final UserGraph user = new UserGraph();
        user.setLogin(login);
        @NotNull final String passwordHash = Optional.ofNullable(HashUtil.salt(propertyService, password))
                .orElseThrow(EmptyPasswordHashException::new);
        user.setPasswordHash(passwordHash);
        add(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserGraph add(
            @Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (isLoginExist(login)) throw new LoginExistException(login);
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isEmailExist(email)) throw new EmailExistException(login);
        @NotNull final UserGraph user = new UserGraph();
        user.setLogin(login);
        user.setEmail(email);
        @NotNull final String passwordHash = Optional.ofNullable(HashUtil.salt(propertyService, password))
                .orElseThrow(EmptyPasswordHashException::new);
        user.setPasswordHash(passwordHash);
        add(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserGraph setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final UserGraph user = findById(id);
        if (user == null) throw new UserNotFoundException();
        @NotNull final String passwordHash = Optional.ofNullable(HashUtil.salt(propertyService, password))
                .orElseThrow(EmptyPasswordHashException::new);
        user.setPasswordHash(passwordHash);
        repository.save(user);
        return user;
    }

    @Override
    @SneakyThrows
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }


    @Override
    @SneakyThrows
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserGraph updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final UserGraph user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        repository.save(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserGraph lockByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserGraph user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        repository.save(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserGraph unlockByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserGraph user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        repository.save(user);
        return user;
    }

}
