package ru.pisarev.tm.client.rest;

import feign.Feign;
import okhttp3.JavaNetCookieJar;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.pisarev.tm.model.Task;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.List;

import static ru.pisarev.tm.constant.SiteConst.site;
import static ru.pisarev.tm.constant.SiteConst.tasks_address;

public interface TaskClient {

    static TaskClient client() {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;

        final CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);

        final okhttp3.OkHttpClient.Builder builder = new okhttp3.OkHttpClient().newBuilder();
        builder.cookieJar(new JavaNetCookieJar(cookieManager));

        return Feign.builder()
                // .client(new OkHttpClient(builder.build()))
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskClient.class, site + tasks_address);
    }

    @GetMapping("/findAll")
    public List<Task> findAll();

    @GetMapping("/find/{id}")
    public Task find(@PathVariable("id") final String id);

    @PostMapping("/create")
    public Task create(@RequestBody final Task task);

    @PostMapping("/createAll")
    public List<Task> createAll(@RequestBody final List<Task> tasks);

    @PutMapping("/save")
    public Task save(@RequestBody final Task task);

    @PutMapping("/saveAll")
    public List<Task> saveAll(@RequestBody final List<Task> tasks);

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") final String id);

    @DeleteMapping("/deleteAll")
    public void deleteAll();

}
