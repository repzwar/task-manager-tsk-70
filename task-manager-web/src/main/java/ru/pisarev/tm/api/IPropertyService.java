package ru.pisarev.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IPropertyService {

    String getApplicationVersion();

    @NotNull String getServerHost();

    @NotNull String getServerPort();

    @Nullable String getJdbcUser();

    @Nullable String getJdbcPassword();

    @Nullable String getJdbcUrl();

    @Nullable String getJdbcDriver();

    @Nullable String getHibernateDialect();

    @Nullable String getHibernateBM2DDLAuto();

    @Nullable String getHibernateShowSql();

    @Nullable String getSecondLevelCash();

    @Nullable String getQueryCache();

    @Nullable String getMinimalPuts();

    @Nullable String getLiteMember();

    @Nullable String getRegionPrefix();

    @Nullable String getCacheProvider();

    @Nullable String getFactoryClass();

}
