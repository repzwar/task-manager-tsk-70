package ru.pisarev.tm.constant;

import org.jetbrains.annotations.NotNull;

public interface PropertyConst {

    @NotNull
    String FILE_NAME = "application.properties";

    @NotNull
    String URL = "url.list";

}
